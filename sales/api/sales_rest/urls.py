from django.urls import path
from .views import (
    api_list_or_create_salesperson,
    api_delete_salesperson,
    api_list_or_create_customer,
    api_delete_customer,
    api_list_or_create_sale,
    api_delete_sale,
)


urlpatterns = [
    path("salespeople/", api_list_or_create_salesperson, name="list_or_create_salesperson"),
    path("salespeople/<int:id>/", api_delete_salesperson, name="delete_salesperson"),
    path("customers/", api_list_or_create_customer, name="list_or_create_customer"),
    path("customers/<int:id>/", api_delete_customer, name="delete_customer"),
    path("sales/", api_list_or_create_sale, name="list_or_create_sale"),
    path("sales/<int:id>/", api_delete_sale, name="delete_sale"),
]
