# CarCar

Team:

* Kyu Sub Shin - Automobile sales
* Justin Queener - Automobile service

## Getting Started

**Ensure you have Docker, Git, and Node.js 20.10.0 or later**

1. Fork this repository

2. Clone the forked repository into a local directory with the command:
```
git clone <<repository.url.here>>
```

3. Build and run the project using Docker from the project top-level directory with the following commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, verify all of your docker containers are running in the desktop application

- View the project in the browser at:
http://localhost:3000/

4. Create a superuser for the services microservice by accessing the service-api-1 container via the Terminal with the following commands:
```
docker exec -it project-beta-ks-jq-service-api-1 bash
python manage.py createsuperuser
```
- Follow the on-screen prompts to create the superuser credentials for the service Django admin.

- In the browser, go to the service Django admin and log in using the created credentials at:
http://localhost:8080/admin/

- Once logged in, click "Statuses" in the left-hand menu under "SERVICE_REST".

- Create 3 statuses with the following configurations as shown in the below screenshot:
```
Id: 1, Name: SCHEDULED
Id: 2, Name: FINISHED
Id: 3, Name: CANCELLED
```
![Img](statuses-creation.png)

- Verify the statuses are correctly created as shown in the below screenshot:

![Img](statuses-verification.png)

## Diagram

![Img](project-beta-ks-jq-diagram.png)

## Design
CarCar is made up of 3 microservices and 2 "pollers" that use RESTful API requests to interact:

The pollers utilize the inventory url: http://project-beta-ks-jq-inventory-api-1:8000/api/automobiles/ to poll for Automobile data from the Inventory microservice and update the AutombileVO value objects needed by their respective microservices.

Respectively, each microservice can be accessed via the following ports from the browswer or insomnia:

- **CarCar**
- **Inventory**    http://localhost:8100/
	- **api**
- **Sales**        http://localhost:8090/
    - **api**
    - **poll**
- **Service**      http://localhost:8080/
    - **api**
    - **poll**

## Value Objects

Both the service and sales microservices have one value object - the AutomobileVO, which contains partial information, namely the VIN, the "sold" status, and the href, of the automobiles in the CarCar inventory. Since the inventory microservice is a separate bounded context, the service and sales microservices can only access the automobile information through an automobile value object (AutomobileVO), which receives data for its aforementioned fields by polling for the automobile data stored in the inventory.

## API Documentation

HIGH LEVEL OVERVIEW

CarCar is comprised of three connected yet largely independent microservices: Inventory, Sales, and Service.

The inventory API allows a user to create and track CarCar automobiles available in its inventory as well as a comprehensive list of their various models and manufacturers.

The sales and service APIs retrieve updated automobile inventory data via their respective, indpendent "pollers". These pollers continuously and periodically send out an API request to the inventory to keep them both updated with the CarCar inventory.

The sales API enables the user to track and create employees, customers, and sales related to CarCar's sales.

The service API enables the user to track and create employees and appointments related to CarCar's automobile service.

## Inventory microservice

### Manufacturers
|  Action                          |  Method  |  URL                                          |
|----------------------------------|----------|-----------------------------------------------|
|  List manufacturers              |  GET     |  http://localhost:8100/api/manufacturers/     |
|  Create a manufacturer           |  POST    |  http://localhost:8100/api/manufacturers/     |
|  Get a specific manufacturer     |  GET     |  http://localhost:8100/api/manufacturers/id/  |
|  Update a specific manufacturer  |  PUT     |  http://localhost:8100/api/manufacturers/id/  |
|  Delete a specific manufacturer  |  DELETE  |  http://localhost:8100/api/manufacturers/id/  |

REQUESTS:

CREATE:
To create a manufacturer, a user must follow the 'create a manufacturer' endpoint. All new manufacturers require a "name" field.

UPDATE:
To update an existing manufacturer, a user must do so by its respective url endpoint with id appended and send the "name" property.

Create and update a manufacturer (SEND THIS JSON BODY):
```
{
	"name": "Chrysler"
}
```
The return data of creating, viewing, and updating a single manufacturer:
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}
```
LIST:
The return data for a list of manufacturers:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		},
	]
}
```
DELETE:
The delete function provides a boolean output. Upon a successful deleting, it returns a null "id" value and the "name" of the manufacturer that was deleted. If the id does not exist, it returns an error message that states that the manufacturer "Does not exist".

The return data for deleting a manufacturer:
```
{
	"id": null,
	"name": "Chrysler"
}
...
{
	"message": "Does not exist"
}
```

### Vehicle models

|  Action                           |  Method  |  URL                                   |
|-----------------------------------|----------|----------------------------------------|
|  List vehicle models              |  GET     |  http://localhost:8100/api/models/     |
|  Create a vehicle model           |  POST    |  http://localhost:8100/api/models/     |
|  Get a specific vehicle model     |  GET     |  http://localhost:8100/api/models/id/  |
|  Update a specific vehicle model  |  PUT     |  http://localhost:8100/api/models/id/  |
|  Delete a specific vehicle model  |  DELETE  |  http://localhost:8100/api/models/id/  |

REQUESTS:

CREATE:
To create a vehicle model, a user must follow the 'create a model' endpoint. All new models require "name", "picture_url", and "manufacturer_id" fields.

UPDATE:
To update an existing vehicle model, a user must do so by its respective url endpoint with id appended and send at least one of the aforementioned properties.

Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Airstream",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/4/46/Chrysler_Airstream_4-Door_Sedan_1936_2.jpg",
  "manufacturer_id": 1
}
```
The return data of creating, viewing, and updating a single vehicle model:
```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Airstream",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/4/46/Chrysler_Airstream_4-Door_Sedan_1936_2.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```
LIST:
The return data for a list of vehicle models:
```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Airstream",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/4/46/Chrysler_Airstream_4-Door_Sedan_1936_2.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```
DELETE:
The delete function provides a boolean output. Upon a successful deleting, it returns a null "id" value and the data associated with the particular vehicle model that was deleted. If the id does not exist, it returns an error message that states that the model "Does not exist".

The return data for deleting a vehicle model:
```
{
	"id": null,
	"name": "Airstream",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/4/46/Chrysler_Airstream_4-Door_Sedan_1936_2.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
...
{
	"message": "Does not exist"
}
```

### Automobiles

|  Action                        |  Method  |  URL                                         |
|--------------------------------|----------|----------------------------------------------|
|  List automobiles              |  GET     |  http://localhost:8100/api/automobiles/      |
|  Create an automobile          |  POST    |  http://localhost:8100/api/automobiles/      |
|  Get a specific automobile     |  GET     |  http://localhost:8100/api/automobiles/vin/  |
|  Update a specific automobile  |  PUT     |  http://localhost:8100/api/automobiles/vin/  |
|  Delete a specific automobile  |  DELETE  |  http://localhost:8100/api/automobiles/vin/  |

REQUESTS:

CREATE:
To create an automobile, a user must follow the 'create an automobile' endpoint. All new automobiles require "color", "year", "vin", "model_id", and "sold" fields. The "sold" property is a Boolean value and should initially be set to false upon automobile creation. However, in the event the autombile is sold, the user may utilize a "PUT" request to update the value to true.

UPDATE:
To update an existing automobile, a user must do so by its respective url endpoint with its VIN appended and send at least one of the aforementioned properties.

Create and update an autombile (SEND THIS JSON BODY):
```
{
	"color": "Silver",
  	"year": 1936,
  	"vin": "7ET8476H7921O97H4",
  	"model_id": 1,
	"sold": false
}
```
Upone creating, viewing or updating an automobile, notice that the "href" returned utilizes the VIN opposed to the ID and that the "manufacturer" property is nested inside the "model" property.

The return data of creating, viewing, and updating a single automobile:
```
{
	"href": "/api/automobiles/7ET8476H7921O97H4/",
	"id": 5,
	"color": "Silver",
	"year": 1936,
	"vin": "7ET8476H7921O97H4",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Airstream",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/4/46/Chrysler_Airstream_4-Door_Sedan_1936_2.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```
LIST:
The return data for a list of automobiles:
```
{
	"autos": [
		{
			"href": "/api/automobiles/7ET8476H7921O97H4/",
			"id": 5,
			"color": "Silver",
			"year": 1936,
			"vin": "7ET8476H7921O97H4",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Airstream",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/4/46/Chrysler_Airstream_4-Door_Sedan_1936_2.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		}
	]
}
```
DELETE:
To delete an autombile, the user must utilize the VIN of the vehicle intended for deletion in its url path. The delete function provides a boolean output. Upon a successful deletion, it returns a null "id" value and the data associated with the particular automobile that was deleted. If the id does not exist, it returns an error message that states that the automobile "Does not exist".

The return data for deleting an automobile:
```
{
	"href": "/api/automobiles/7ET8476H7921O97H4/",
	"id": null,
	"color": "Silver",
	"year": 1936,
	"vin": "7ET8476H7921O97H4",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Airstream",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/4/46/Chrysler_Airstream_4-Door_Sedan_1936_2.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
...
{
	"message": "Does not exist"
}
```

## Service microservice

### Technicians

|  Action                        |  Method  |  URL                                       |
|--------------------------------|----------|--------------------------------------------|
|  List technicians              |  GET     |  http://localhost:8080/api/technicians/    |
|  Create a technician           |  POST    |  http://localhost:8080/api/technicians/    |
|  Get a specific technician     |  GET     |  http://localhost:8080/api/technicians/id/ |
|  Update a specific technician  |  PUT     |  http://localhost:8080/api/technicians/id/ |
|  Delete a specific technician  |  DELETE  |  http://localhost:8080/api/technicians/id/ |

JSON body to send data:

CREATE:
To create a technician, a user must follow the 'create a technician' endpoint. All new technicians require a "first_name", "last_name", and "employee_id" field.

UPDATE:
To update an existing technician, a user must do so by its respective url endpoint and send at least one of the aforementioned properties.

Create and update a technician (SEND THIS JSON BODY):
```
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "123456"
}
```
The return data of creating, viewing, and updating a single technician:
```
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "123456",
	"id": 1
}
```
LIST:
The return data for a list of technicians:
```
{
	"technicians": [
		{
			"first_name": "John",
			"last_name": "Doe",
			"employee_id": "123456",
			"id": 1
		},
	]
}
```
DELETE:
To delete a technician, the user must append the id to the url endpoint. The resulting return data provides a boolean output. Upon a successful deletion, the result is true. Attempting to delete an already deleted or non-existent technician will return false.

The return data for deleting a technician:
```
{
	"deleted": true
}
...
{
	"deleted": false
}

```

### Appointments

|  Action                         |  Method  |  URL                                               |
|---------------------------------|----------|----------------------------------------------------|
|  List appointments              |  GET     |  http://localhost:8080/api/appointments/           |
|  Create an appointment          |  POST    |  http://localhost:8080/api/appointments/           |
|  Finish a specific appointment  |  PUT     |  http://localhost:8080/api/appointments/id/finish/ |
|  Cancel a specific appointment  |  PUT     |  http://localhost:8080/api/appointments/id/cancel/ |
|  Delete a specific appointment  |  DELETE  |  http://localhost:8080/api/appointments/id/        |

JSON body to send data:

CREATE:
To create an appointment, a user must follow the 'create an appointment' endpoint. All new appointments require a "date_time", "reason", "vin", "customer", and "technician" field. Notice that all fields, including numeric ones, are sent as strings and the technician is identified by their id number rather than their name.

Create an appointment (SEND THIS JSON BODY):
```
{
	"date_time": "2023-12-25",
	"reason": "Leaky windshield",
	"vin": "7E5GF8JC4C1353408",
	"customer": "Lewis Marakowitz",
	"technician": "123456"
}
```
The returned data on a newly created appointment also returns all associated data for the assigned technician as well as a "status" property. By default, every newly created appointment is set to "SCHEDULED". For information on changing the status, please see the FINISH and CANCEL documentation.

The return data of creating an appointment:
```
{
	"id": 15,
	"date_time": "2023-12-25",
	"reason": "Leaky windshield",
	"vin": "7E5GF8JC4C1353408",
	"customer": "Lewis Marakowitz",
	"technician": {
		"first_name": "John",
		"last_name": "Doe",
		"employee_id": "123456",
		"id": 1
	},
	"status": "SCHEDULED"
}
```
LIST:
The return data for a list of appointments:
```
{
	"appointments": [
		{
			"id": 15,
			"date_time": "2023-12-25T00:00:00+00:00",
			"reason": "Leaky windshield",
			"vin": "7E5GF8JC4C1353408",
			"customer": "Lewis Marakowitz",
			"technician": {
				"first_name": "John",
				"last_name": "Doe",
				"employee_id": "123456",
				"id": 1
			},
			"status": "SCHEDULED"
		},
	]
}
```
FINISH:
The "Finish an appointment" endpoint utilizes the "PUT" method to change the status property on an appointment. As such, the request can be sent by only providing the status property to the "PUT" method as shown below.

Finish an appointment (SEND THIS JSON BODY):
```
{
	"status": "FINISHED"
}
```
CANCEL:
Indentically to the FINISH method, the "Cancel an appointment" endpoint utilizes the "PUT" method to change the status property on an appointment and the JSON body can be provided in a similar manner.

Cancel an appointment (SEND THIS JSON BODY):
```
{
	"status": "CANCELLED"
}
```
DELETE:
To delete an appointment, the user must append the id to the url endpoint. Upon a successful deletion, the result will be the JSON data for the deleted appointment. Attempting to delete an already deleted or non-existent appointment will return an error message stating that the "Appointment does not exist".

The return data for deleting an appointment:
```
{
	"id": null,
	"date_time": "2023-12-29T00:00:00+00:00",
	"reason": "Headlight out",
	"vin": "76553210987654323",
	"customer": "Phil Micah Stanbury",
	"technician": {
		"first_name": "John",
		"last_name": "Doe",
		"employee_id": "123456",
		"id": 1
	},
	"status": "CANCELLED"
}
...
{
	"message": "Appointment does not exist"
}
```

## Sales microservice

### Salespeople

|  Action                         |  Method  |  URL                                        |
|---------------------------------|----------|---------------------------------------------|
|  List salespeople               |  GET     |  http://localhost:8090/api/salespeople/     |
|  Create a salesperson           |  POST    |  http://localhost:8090/api/salespeople/     |
|  Delete a specific salesperson  |  DELETE  |  http://localhost:8090/api/salespeople/id/  |

LIST:
The return data for a list of salespeople:
```
{
	"salespeople": [
		{
			"first_name": "Helder",
			"last_name": "Forlan",
			"employee_id": "1W2X3Y4Z",
			"id": 1
		}
	]
}
```
CREATE:
To create a salesperson, a user must follow the 'create a salesperson' endpoint. All new salespeople require "first_name", "last_name", and "employee_id" fields.

Create a salesperson (SEND THIS JSON BODY):
```
{
	"first_name": "Helder",
	"last_name": "Forlan",
	"employee_id": "1W2X3Y4Z"
}
```
The return data of creating a single salesperson:
```
{
	"first_name": "Helder",
	"last_name": "Forlan",
	"employee_id": "1W2X3Y4Z",
	"id": 1
}
```
DELETE:
To delete a salesperson, the user must append the salesperson's id to the url endpoint. The resulting return data provides a boolean output. Upon a successful deletion, the result is true. Attempting to delete an already deleted or non-existent salesperson will return false.

The return data for deleting a salesperson:
```
{
	"deleted": true
}
...
{
	"deleted": false
}

```

### Customers

|  Action                      |  Method  |  URL                                      |
|------------------------------|----------|-------------------------------------------|
|  List customers              |  GET     |  http://localhost:8090/api/customers/     |
|  Create a customer           |  POST    |  http://localhost:8090/api/customers/     |
|  Delete a specific customer  |  DELETE  |  http://localhost:8090/api/customers/id/  |

LIST:
The return data for a list of customers:
```
{
	"customers": [
		{
			"first_name": "Andrew",
			"last_name": "Shaw",
			"address": "842 Red Ln, Lonchester, MA 09876",
			"phone_number": 3456780921,
			"id": 12
		}
	]
}
```
CREATE:
To create a customer, a user must follow the 'create a customer' endpoint. All new customers require "first_name", "last_name", "address", and "phone_number" fields.

Create a customer (SEND THIS JSON BODY):
```
{
	"first_name": "Andrew",
	"last_name": "Shaw",
	"address": "842 Red Ln, Lonchester, MA 09876",
	"phone_number": "3456780921"
}
```
The return data of creating a single customer:
```
{
	"first_name": "Andrew",
	"last_name": "Shaw",
	"address": "842 Red Ln, Lonchester, MA 09876",
	"phone_number": "3456780921",
	"id": 12
}
```
DELETE:
To delete a customer, the user must append the customer's id to the url endpoint. The resulting return data provides a boolean output. Upon a successful deletion, the result is true. Attempting to delete an already deleted or non-existent customer will return false.

The return data for deleting a customer:
```
{
	"deleted": true
}
...
{
	"deleted": false
}

```

### Sales

|  Action                  |  Method  |  URL                                  |
|--------------------------|----------|---------------------------------------|
|  List sales              |  GET     |  http://localhost:8090/api/sales/     |
|  Create a sale           |  POST    |  http://localhost:8090/api/sales/     |
|  Delete a specific sale  |  DELETE  |  http://localhost:8090/api/sales/id/  |

LIST:
The return data for a list of sales:
```
{
	"sales": [
		{
			"automobile": {
				"vin": "2C2DC4GB1B9030092",
				"sold": true,
				"import_href": "/api/automobiles/2C2DC4GB1B9030092/"
			},
			"salesperson": {
				"first_name": "Gil",
				"last_name": "Neves",
				"employee_id": "2Y5X3B6A",
				"id": 14
			},
			"customer": {
				"first_name": "Andrew",
				"last_name": "Shaw",
				"address": "842 Red Ln, Lonchester, MA 09876",
				"phone_number": 3456780921,
				"id": 12
			},
			"price": "87654.00",
			"id": 14
		}
	]
}
```
CREATE:
To create a sale, a user must follow the 'create a sale' endpoint. All new sales require "automobile", "salesperson", "customer", and "price" fields.

Create a sale (SEND THIS JSON BODY):
```
{
	"automobile": "2C2DC4GB1B9030092",
	"salesperson": 14,
	"customer": 12,
	"price": 87654.00
}
```
The return data of creating a single sale:
```
{
	"automobile": {
		"vin": "2C2DC4GB1B9030092",
		"sold": false,
		"import_href": "/api/automobiles/2C2DC4GB1B9030092/"
	},
	"salesperson": {
		"first_name": "Gil",
		"last_name": "Neves",
		"employee_id": "2Y5X3B6A",
		"id": 14
	},
	"customer": {
		"first_name": "Andrew",
		"last_name": "Shaw",
		"address": "842 Red Ln, Lonchester, MA 09876",
		"phone_number": 3456780921,
		"id": 12
	},
	"price": 87654.0,
	"id": 14
}
```
DELETE:
To delete a sale, the user must append the sale's id to the url endpoint. The resulting return data provides a boolean output. Upon a successful deletion, the result is true. Attempting to delete an already deleted or non-existent sale will return false.

The return data for deleting a sale:
```
{
	"deleted": true
}
...
{
	"deleted": false
}

```
