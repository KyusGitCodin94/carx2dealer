import { useEffect, useState } from 'react';

function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments.filter((appointment) => appointment["status"] === "SCHEDULED"))
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const handleFinish = async (id) => {
        const request = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {method: "PUT"});
        const response = await request.json()
        getData();
    }

    const handleCancel = async (id) => {
        const request = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {method: "PUT"});
        const response = await request.json()
        getData();
    }

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/automobiles/');

        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const checkVip = (vin) => {
        return automobiles.some(function(auto) {
            if (auto.vin === vin) {
                return auto.sold
            }
        })
    }

    return (
    <div>
        <div>
            <h1>
                Service Appointments
            </h1>
        </div>
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Finish/Cancel</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                        <td>{ appointment.vin }</td>
                        {(checkVip(appointment.vin)) ? <td>Yes</td>:<td>No</td>}
                        <td>{ appointment.customer }</td>
                        <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                        <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                        <td>{ `${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                        <td>{ appointment.reason }</td>
                        <td>
                            <button onClick={()=>{
                                handleFinish(appointment.id)
                            }} className="btn btn-success">Finish</button>
                            <button onClick={()=>{
                                handleCancel(appointment.id)
                            }} className="btn btn-danger">Cancel</button>
                        </td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    </div>
    )
}

export default AppointmentsList;
